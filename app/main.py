import os
from flask import Flask, render_template, request, redirect, url_for, flash
import numpy as np
from scipy import stats
from scipy.fft import rfft, rfftfreq
from matplotlib import pyplot as plt
import pywt
from keras.models import load_model

path = os.getcwd()
UPLOAD_FOLDER = os.path.join(path, 'uploads')
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/')
@app.route('/hello')
def hello():
    return "Hello"


@app.route('/upload')
def upload_form():
    return render_template('upload.html')


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        analysis_method = request.form['analysis_method']
        file = request.files['file']
        filename = file.filename
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return redirect(url_for('result', filename=filename, analysis_method=analysis_method))


@app.route('/upload_fft')
def upload_fft():
    return render_template('upload_fft.html')


@app.route('/upload_fft', methods=['GET', 'POST'])
def upload_file_fft():
    if request.method == 'POST':
        upper_limit = request.form['upper_limit']
        lower_limit = request.form['lower_limit']
        baseline_file = request.files['baseline_file']
        current_file = request.files['current_file']
        baseline_name = baseline_file.filename
        current_name = current_file.filename
        baseline_file.save(os.path.join(app.config['UPLOAD_FOLDER'], baseline_name))
        current_file.save(os.path.join(app.config['UPLOAD_FOLDER'], current_name))
        return redirect(url_for('result_fft', baseline_name=baseline_name, current_name=current_name, upper_limit=upper_limit, lower_limit=lower_limit))


@app.route('/result/', methods=['GET', 'POST'])
@app.route('/result/<filename>/<analysis_method>/', methods=['GET', 'POST'])
def result(filename, analysis_method):

    f = open(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    y = np.loadtxt(f, delimiter=',', dtype=float)

    if analysis_method == 'cwt':
        N = len(y)
        time = np.arange(0, N)
        scales = np.arange(1, 128)
        waveletname = 'cgau8'

        coefficients, frequencies = pywt.cwt(y, scales, waveletname)

        fig_name = _plot_wavelet(time, coefficients, frequencies, filename, analysis_method)
        return render_template('result.html', user_image=fig_name)

    if analysis_method == 'dwt':
        waveletname = 'db5'

        fig, axarr = plt.subplots(nrows=5, ncols=2, figsize=(18, 18))
        for ii in range(5):
            (y, coeff_d) = pywt.dwt(y, waveletname)
            axarr[ii, 0].plot(y, 'r')
            axarr[ii, 1].plot(coeff_d, 'g')
            axarr[ii, 0].set_ylabel("Level {}".format(ii + 1), fontsize=14, rotation=90)
            axarr[ii, 0].set_yticklabels([])
            if ii == 0:
                axarr[ii, 0].set_title("Approximation coefficients", fontsize=14)
                axarr[ii, 1].set_title("Detail coefficients", fontsize=14)
            axarr[ii, 1].set_yticklabels([])
        plt.tight_layout()

        fig_name = os.path.splitext(filename)[0] + analysis_method + '.png'
        fig_path = os.path.join('./static/', fig_name)
        plt.savefig(fig_path)
        plt.close()
        return render_template('result.html', user_image=fig_name)

    if analysis_method == 'ssa':
        windowLen = 50
        seriesLen = len(y)
        K = seriesLen - windowLen + 1
        X = np.zeros((windowLen, K))
        for i in range(K):
            X[:, i] = y[i:i + windowLen]

        U, sigma, VT = np.linalg.svd(X, full_matrices=False)

        for i in range(VT.shape[0]):
            VT[i, :] *= sigma[i]
        A = VT

        rec = np.zeros((windowLen, seriesLen))
        for i in range(windowLen):
            for j in range(windowLen - 1):
                for m in range(j + 1):
                    rec[i, j] += A[i, j - m] * U[m, i]
                rec[i, j] /= (j + 1)
            for j in range(windowLen - 1, seriesLen - windowLen + 1):
                for m in range(windowLen):
                    rec[i, j] += A[i, j - m] * U[m, i]
                rec[i, j] /= windowLen
            for j in range(seriesLen - windowLen + 1, seriesLen):
                for m in range(j - seriesLen + windowLen, windowLen):
                    rec[i, j] += A[i, j - m] * U[m, i]
                rec[i, j] /= (seriesLen - j)

        # rrr = np.sum(rec, axis=0)

        for i in range(4):
            ax = plt.subplot(4, 1, i + 1)
            ax.plot(rec[i, :])

        fig = plt.gcf()
        fig.tight_layout()

        fig_name = os.path.splitext(filename)[0] + analysis_method + '.png'
        fig_path = os.path.join('./static/', fig_name)
        plt.savefig(fig_path)
        plt.close()

        return render_template('result.html', user_image=fig_name)

    if analysis_method == 'cwt_nn':
        fig_names = []
        data_cwt = np.ndarray(shape=(1, 127, y.shape[0], y.shape[1]))
        scales = range(1, 128)
        waveletname = 'shan'
        for i in range(y.shape[1]):
            signal = y[:, i]
            coeff, freq = pywt.cwt(signal, scales, waveletname, 1)
            data_cwt[0, :, :, i] = coeff

            N = len(signal)
            time = np.arange(0, N)
            fig_name = _plot_wavelet(time, coeff, freq, filename, analysis_method)
            fig_names.append(fig_name)

        model_path = os.path.join('./models/', 'cwt_nn_model.h5')
        model = load_model(model_path)
        predictions = model.predict(data_cwt)
        classes = np.argmax(predictions, axis=1)

        return render_template('result_cwtnn.html', image_1=fig_names[0], image_2=fig_names[1], image_3=fig_names[2], classes=classes)


@app.route('/result_fft/', defaults={'upper_limit': None, 'lower_limit': None}, methods=['GET', 'POST'])
@app.route('/result_fft/<baseline_name>/<current_name>/<upper_limit>/<lower_limit>/', methods=['GET', 'POST'])
def result_fft(baseline_name, current_name, upper_limit, lower_limit):
    f_baseline = open(os.path.join(app.config['UPLOAD_FOLDER'], baseline_name))
    y_baseline = np.loadtxt(f_baseline, dtype=float)

    f_current = open(os.path.join(app.config['UPLOAD_FOLDER'], current_name))
    y_current = np.loadtxt(f_current, dtype=float)

    yf_baseline = rfft(y_baseline)
    xf_baseline = rfftfreq(len(y_baseline), 1 / len(y_baseline))
    plt.plot(xf_baseline, np.abs(yf_baseline))
    fig_name_baseline = os.path.splitext(baseline_name)[0] + '.png'
    fig_path_baseline = os.path.join('./static/', fig_name_baseline)
    plt.savefig(fig_path_baseline)
    plt.close()

    yf_current = rfft(y_current)
    xf_current = rfftfreq(len(y_current), 1 / len(y_current))
    plt.plot(xf_current, np.abs(yf_current))
    if upper_limit is not None:
        plt.axvline(x=upper_limit, linestyle="--", color='red')
        upper_limit = float(upper_limit)
    if lower_limit is not None:
        plt.axvline(x=lower_limit, linestyle="--", color='red')
        lower_limit = float(lower_limit)
    fig_name_current = os.path.splitext(current_name)[0] + '.png'
    fig_path_current = os.path.join('./static/', fig_name_current)
    plt.savefig(fig_path_current)
    plt.close()

    similarity = stats.pearsonr(np.abs(yf_baseline), np.abs(yf_current))[0]
    max_freq = xf_current[np.argmax(np.abs(yf_current))]

    status = 'abnormal'
    if similarity > 0.5:
        status = 'normal'

    if isinstance(lower_limit, float) and isinstance(upper_limit, float):
        if lower_limit < max_freq < upper_limit:
            status = 'normal'

    return render_template('result_fft.html', baseline_image=fig_name_baseline, current_image=fig_name_current,
                           lower_limit=lower_limit, upper_limit=upper_limit,
                           max_freq=max_freq, similarity=similarity, status=status)


def _plot_wavelet(time, coefficients, frequencies, filename, analysis_method,
                 cmap=plt.cm.seismic,
                 title='Wavelet Transform (Power Spectrum) of signal',
                 ylabel='Period',
                 xlabel='Time'):
    power = (abs(coefficients)) ** 2
    period = 1. / frequencies
    levels = [0.0625, 0.125, 0.25, 0.5, 1, 2, 4, 8]
    contourlevels = np.log2(levels)

    fig, ax = plt.subplots(figsize=(15, 10))
    im = ax.contourf(time, np.log2(period), np.log2(power), contourlevels, extend='both', cmap=cmap)

    ax.set_title(title, fontsize=20)
    ax.set_ylabel(ylabel, fontsize=18)
    ax.set_xlabel(xlabel, fontsize=18)

    yticks = 2 ** np.arange(np.ceil(np.log2(period.min())), np.ceil(np.log2(period.max())))
    ax.set_yticks(np.log2(yticks))
    ax.set_yticklabels(yticks)
    ax.invert_yaxis()
    ylim = ax.get_ylim()
    ax.set_ylim(ylim[0], -1)

    cbar_ax = fig.add_axes([0.95, 0.5, 0.03, 0.25])
    fig.colorbar(im, cax=cbar_ax, orientation="vertical")

    fig_name = os.path.splitext(filename)[0] + analysis_method + '.png'
    fig_path = os.path.join('./static/', fig_name)
    plt.savefig(fig_path)
    plt.close()
    return fig_name


if __name__ == '__main__':
    app.secret_key = 'secret_key'
    app.debug = True
    app.run(host='0.0.0.0', port=5000)

