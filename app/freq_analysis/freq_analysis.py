import numpy as np
from scipy.fft import rfft, rfftfreq


def _fft(path):
    f = open(path)
    y = np.loadtxt(f, dtype=float)
    yf = rfft(y)
    xf = rfftfreq(len(y), 1 / len(y))
    plt.plot(xf, np.abs(yf))
    plt.savefig('test1.png')
    plt.close()
